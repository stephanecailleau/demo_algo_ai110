package fr.eql.ai110.tpvins;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class WineManager {

	private String SourceFilePath = "C:/toto/TPVINS.DON";
	private String DestinationFilePath = "C:/toto/TP.RAF";

	private int tailleChampNom = 0;
	private int tailleChampAppellation = 0;
	private int tailleChampRegion = 0;
	private int tailleChampProprio = 0;
	private int tailleChampSurface = 0;
	private int tailleChampNumStand = 0;

	private int tailleEnregistrement;
	private int nombreDeVins = 0;

	private int indexCurrentCharInSourceLine;

	/**
	 * Extrait le champ suivant dans la ligne source (lecture jusqu'� la prochaine
	 * tabulation)
	 * 
	 * @param lineChars ligne en cours de lecture
	 * @return valeur du champ lu
	 */
	private String extractNextFieldInSourceLine(char[] lineChars) {

		String result = "";

		char c = lineChars[indexCurrentCharInSourceLine];
		while (c != '\t') {
			indexCurrentCharInSourceLine++;
			result += c;
			// � la fin d'une ligne, on ne doit pas lire le suivant (car il n'y en n'a pas)
			if (indexCurrentCharInSourceLine < lineChars.length) {
				c = lineChars[indexCurrentCharInSourceLine];
			} else {
				break;
			}
		}
		indexCurrentCharInSourceLine++;

		return result;
	}

	/**
	 * Parcourt le fichier cible pour : - remplir une liste de vins (objets) -
	 * d�finir la taille des champs
	 * 
	 * @return Liste des vins du fichier
	 */
	private List<Vin> readSourceFile() {

		List<Vin> wines = new ArrayList<Vin>();

		try {
			BufferedReader source = new BufferedReader(new FileReader(SourceFilePath));

			// pour chaque ligne du fichier source :
			while (source.ready()) {
				String line = source.readLine();

				char[] lineChars = line.toCharArray();

				// Ch�teau Sainte-H�l�ne Sauternes Bordelais Dmu 42 U92

				indexCurrentCharInSourceLine = 0;

				// r�cup�rer les champs de la ligne :
				String nom = extractNextFieldInSourceLine(lineChars);
				String appellation = extractNextFieldInSourceLine(lineChars);
				String region = extractNextFieldInSourceLine(lineChars);
				String proprio = extractNextFieldInSourceLine(lineChars);
				String surface = extractNextFieldInSourceLine(lineChars);
				String numStand = extractNextFieldInSourceLine(lineChars);

				// mettre les tailles de champs :
				if (nom.length() > tailleChampNom)
					tailleChampNom = nom.length();
				if (appellation.length() > tailleChampAppellation)
					tailleChampAppellation = appellation.length();
				if (region.length() > tailleChampRegion)
					tailleChampRegion = region.length();
				if (proprio.length() > tailleChampProprio)
					tailleChampProprio = proprio.length();
				if (surface.length() > tailleChampSurface)
					tailleChampSurface = surface.length();
				if (numStand.length() > tailleChampNumStand)
					tailleChampNumStand = numStand.length();

				// cr�er un objet Vin :
				Vin vin = new Vin(nom, appellation, region, proprio, surface, numStand);

				// ajout de l'objet vin � la liste
				wines.add(vin);
				nombreDeVins++;
			}

			tailleEnregistrement = tailleChampNom + tailleChampAppellation + tailleChampRegion + tailleChampProprio
					+ tailleChampSurface + tailleChampNumStand;

		} catch (FileNotFoundException e) {
			System.err.println("Fichier source introuvable !");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Erreur de lecture du fichier source !");
			e.printStackTrace();
		}

		return wines;
	}

	public void formatRafData() {

		List<Vin> wines = readSourceFile();

		for (Vin vin : wines) {
			System.out.println(vin);
		}

		System.out.println("Nb de vins : " + wines.size());

		System.out.println("taille nom : " + tailleChampNom);
		System.out.println("taille appellation : " + tailleChampAppellation);
		System.out.println("taille region : " + tailleChampRegion);
		System.out.println("taille proprio : " + tailleChampProprio);
		System.out.println("taille surface : " + tailleChampSurface);
		System.out.println("taille numStand : " + tailleChampNumStand);

		writeDestinationFile(wines);
		sortDestinationFile();
	}

	/**
	 * Cr�e un fichier structur� pour un acc�s direct, contenant la liste des vins
	 * 
	 * @param wines liste d'objets Vin � �crire dans le fichier
	 */
	private void writeDestinationFile(List<Vin> wines) {

		// d�clarer et ouvrir le raf
		try {
			RandomAccessFile raf = new RandomAccessFile(this.DestinationFilePath, "rw");

			for (Vin vin : wines) {
				writeWine(vin, raf);
			}

			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Ecrit un enregistrement dans le fichier des vins � l'emplacement actuel du
	 * curseur
	 * 
	 * @param vin
	 * @param raf
	 */
	private void writeWine(Vin vin, RandomAccessFile raf) {

		writeWineField(vin.getNom(), tailleChampNom, raf);
		writeWineField(vin.getAppellation(), tailleChampAppellation, raf);
		writeWineField(vin.getRegion(), tailleChampRegion, raf);
		writeWineField(vin.getProprio(), tailleChampProprio, raf);
		writeWineField(vin.getSurface(), tailleChampSurface, raf);
		writeWineField(vin.getNumeroStand(), tailleChampNumStand, raf);

	}

	/**
	 * Ecrit un enregistrement dans le fichier des vins � l'emplacement demand�
	 * 
	 * @param vin
	 * @param raf
	 * @param index emplacement ou je souhaite �crire mon vin
	 */
	private void writeWine(Vin vin, RandomAccessFile raf, int index) {

		try {
			// je me positionne � l'index souhait� dans le fichier :
			raf.seek(index * tailleEnregistrement);

			// j'�cris le vin � partir du curseur positionn�
			writeWine(vin, raf);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Ecrit un champ d'un enregistrement dans le fichier des vins
	 * 
	 * @param value
	 * @param taille
	 * @param raf
	 */
	private void writeWineField(String value, int taille, RandomAccessFile raf) {

		try {
			// �crire le champ
			raf.write(value.getBytes("ISO_8859_1"));

			// compl�ter avec des espaces
			int nbEspacesRestants = taille - value.length();

			for (int i = 0; i < nbEspacesRestants; i++) {
				raf.write(" ".getBytes("ISO_8859_1"));
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * R�cup�re un objet vin depuis les fichier, � l'index demand�
	 * 
	 * @param index
	 * @param raf
	 * @return
	 */
	private Vin readWine(int index, RandomAccessFile raf) {
		Vin vin = new Vin();

		try {
			// je me place au d�but de l'enregistrement :
			raf.seek(index * tailleEnregistrement);

			// lire chacun des champs :
			vin.setNom(readWineField(tailleChampNom, raf));
			vin.setAppellation(readWineField(tailleChampAppellation, raf));
			vin.setRegion(readWineField(tailleChampRegion, raf));
			vin.setProprio(readWineField(tailleChampProprio, raf));
			vin.setSurface(readWineField(tailleChampSurface, raf));
			vin.setNumeroStand(readWineField(tailleChampNumStand, raf));

		} catch (IOException e) {
			e.printStackTrace();
		}

		return vin;
	}

	private String readWineField(int taille, RandomAccessFile raf) {
		String result = "";
		try {
			byte[] b = new byte[taille];
			raf.read(b, 0, taille);
			result = new String(b, StandardCharsets.ISO_8859_1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Trie le fichier des vins par nom
	 * 
	 * @param raf
	 */
	private void sortDestinationFile() {
		try {
			RandomAccessFile raf;

			raf = new RandomAccessFile(this.DestinationFilePath, "rw");

			for (int borneSuperieure = nombreDeVins; borneSuperieure > 0; borneSuperieure--) {
				for (int indexCourant = 0; indexCourant < borneSuperieure - 1; indexCourant++) {
					// int elementCourant = t[indexCourant];
					Vin elementCourant = readWine(indexCourant, raf);
					Vin elementSuivant = readWine(indexCourant + 1, raf);

					if (elementCourant.getNom().compareTo(elementSuivant.getNom()) > 0) {
						permuter(indexCourant, indexCourant + 1, raf);
					}
				}
			}

			raf.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void permuter(int a, int b, RandomAccessFile raf) {
		// je r�cup�re les 2 objets � permuter (dans le fichier) :
		Vin vinA = readWine(a, raf);
		Vin vinB = readWine(b, raf);
		// je permute a et b :
		writeWine(vinA, raf, b);
		writeWine(vinB, raf, a);
	}

	/** recherche d'un vin par dichotomie
	 * 
	 * @param name nom du vin � trouver
	 * @return
	 */
	public Vin searchByName(String name) {
		
		Vin result = null;
		
		try {
			
			RandomAccessFile raf = new RandomAccessFile(this.DestinationFilePath, "rw");
		
			result = dichotomicSearch(name, 0, nombreDeVins, raf);
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public Vin dichotomicSearch(String name, int start, int end, RandomAccessFile raf) {
		
		// je vais voir au milieu :
		int middle = (start + end) / 2;
		
		Vin middleWine = readWine(middle, raf);
		
		String currentName = formatWineName(middleWine.getNom());
		String searchName = formatWineName(name);
		
		if (currentName.equals(searchName)) {
			// j'ai trouv� mon vin :
			return middleWine;
		}
		else
		{
			if (start == end) {
				return null;
			}
			else if (currentName.compareTo(searchName) < 0) {
				// middle est plus petit que name
				return dichotomicSearch(name, middle+1, end, raf);
			}
			else
			{	
				// middle est plus grand que name
				return dichotomicSearch(name, start, middle-1, raf);
			}
		}
	}

	private String formatWineName(String name) {
		String result = name;
		
		result = result.trim();
		result = result.toLowerCase();
		result = result.replace('�', 'a');
		result = result.replace('�', 'e');
		result = result.replace('�', 'e');
		result = result.replace('�', 'e');
		result = result.replace('-', ' ');
		
		return result;
	}

}
