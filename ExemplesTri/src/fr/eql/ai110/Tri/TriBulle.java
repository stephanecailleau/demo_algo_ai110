package fr.eql.ai110.Tri;

public class TriBulle {

	public static void main(String[] args) {
		
		int[] tableau = {3,5,1,4,9,7,2,8};
		
		//bubbleSort(tableau);
		bubbleSortRecursif(tableau, tableau.length);
		
		for (int i = 0; i < tableau.length; i++) {
			System.out.print(tableau[i] + " ");
		}
	}
	
	public static void bubbleSortRecursif(int[] t, int borneSuperieure)
	{
		if (borneSuperieure > 0){
			for (int indexCourant = 0; indexCourant < borneSuperieure-1; indexCourant++)
			{
				int elementCourant = t[indexCourant];
				int elementSuivant = t[indexCourant+1];
				
				if( elementCourant > elementSuivant) {
					permuter(indexCourant, indexCourant +1, t);
				}
			}			
			bubbleSortRecursif(t, borneSuperieure - 1);
		}		
	}
	
	
	public static void bubbleSort(int[] t)
	{
		for (int borneSuperieure = t.length; borneSuperieure > 0; borneSuperieure--)
		{				
			for (int indexCourant = 0; indexCourant < borneSuperieure-1; indexCourant++)
			{
				int elementCourant = t[indexCourant];
				int elementSuivant = t[indexCourant+1];
				
				if( elementCourant > elementSuivant) {
					permuter(indexCourant, indexCourant +1, t);
				}
			}
		}
	}

	private static void permuter(int i, int j, int[] t) {		
		int buffer = t[i];
		t[i] = t[j];
		t[j] = buffer;		
	}
	
}
