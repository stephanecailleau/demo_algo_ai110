package fr.eql.ai110.justification;

public class Justificator {
	
	// 1 - d�clarer les attributs
	private int longueurLigne = 80;
	private int longueurMargeGauche;
	private int longueurMargeDroite;
	private char[] texteAJustifier;
	private int nombreLettres;
	private int nombreMots;
	private int nombreEspacesAAfficher;
	private int nombreSeparateurs;
	private int longueurSeparateur;
	private int resteEspacesAAllouer;
	private int nbSeparateursRestants;
	// constantes :
	private final static char CARACTERE_SEPARATEUR = ' ';
	private final static char CARACTERE_RESTE_A_ALLOUER = ' ';
	private final static char CARACTERE_MARGE = ' ';
	
	// 2 - identifier les m�thodes
	public void justifier(String texte, int longueurMargeGauche, int longueurMargeDroite) {
		
		initialiser(texte, longueurMargeGauche, longueurMargeDroite);
		
		definirLongueurSeparateur();
	
		afficherTexteJustifie();
	}
	
	private void afficherTexteJustifie() {

		afficherMarge(this.longueurMargeGauche);
		
		afficherLigneJustifiee();
		
		afficherMarge(this.longueurMargeDroite);
	}



	private void afficherLigneJustifiee() {

		boolean precedentEstVide = true;
		nbSeparateursRestants = nombreSeparateurs;
		
		for (int i = 0; i < texteAJustifier.length; i++)
		{
			char courant = texteAJustifier[i];
			
			if (courant != ' ') {
				System.out.print(courant);
				precedentEstVide = false;
			}
			else {
				boolean afficherUnSeparateur = (precedentEstVide == false) && (nbSeparateursRestants > 0);

				if (afficherUnSeparateur) {
					afficherSeparateur();
				}				
				precedentEstVide = true;
			}			
		}
	}

	private void afficherSeparateur() {		
		for (int i = 0; i < longueurSeparateur; i++)
		{
			System.out.print(CARACTERE_SEPARATEUR);
		}
		
		if (resteEspacesAAllouer > 0) {
			System.out.print(CARACTERE_RESTE_A_ALLOUER);
			resteEspacesAAllouer--;
		}
		nbSeparateursRestants--;
	}

	private void afficherMarge(int longueur) {
		
		for (int i = 0; i < longueur; i++) {
			System.out.print(CARACTERE_MARGE);
		}
		
	}

	private void definirLongueurSeparateur() {

		definirNombreLettres();
		
		definirNombreMots();
		
		nombreSeparateurs = nombreMots - 1;
		
		nombreEspacesAAfficher = longueurLigne - nombreLettres;
		
		longueurSeparateur = nombreEspacesAAfficher / nombreSeparateurs;				
		
		resteEspacesAAllouer = nombreEspacesAAfficher % nombreSeparateurs;
		
		System.out.println("nb separateurs : " + nombreSeparateurs);
		System.out.println("nb espaces � afficher : " + nombreEspacesAAfficher);
		System.out.println("longueur separateur : " + longueurSeparateur);
		System.out.println("reste � allouer : " + resteEspacesAAllouer);
	}


	private void definirNombreMots() {
		
		boolean precedentEstVide = true;
		nombreMots = 0;
		
		for (int i = 0; i < texteAJustifier.length; i++)
		{
			char courant = texteAJustifier[i];
			
			boolean premiereLettreDeMot = (courant != ' ') && (precedentEstVide == true);
			if (premiereLettreDeMot) {
				nombreMots++;
			}
			
			if (courant == ' ') {
				precedentEstVide = true;
			}
			else
			{
				precedentEstVide = false;
			}	
		}
		
		System.out.println("nombre de mots : " + this.nombreMots);	
	}


	private void definirNombreLettres() {
		
		this.nombreLettres = 0;
		
		for (int i = 0; i < this.texteAJustifier.length; i++) {
			
			char courant = this.texteAJustifier[i];
			
			if (courant != ' ') {
				this.nombreLettres++;
			}
		}
		
		System.out.println("Nombre de lettres : " + this.nombreLettres);
	}


	private void initialiser(String texte, int longueurMargeGauche, int longueurMargeDroite) {
		
		// conversion de String en char[] :
		this.texteAJustifier = texte.toCharArray();
		this.longueurMargeGauche = longueurMargeGauche;
		this.longueurMargeDroite = longueurMargeDroite;
		
	}
}







